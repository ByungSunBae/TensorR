# classify MNIST dataset using nearest neighbor with tensorflow
# reference : https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/2_BasicModels/random_forest.py
library(magrittr)
library(tensorflow)
library(keras)
use_python("/home/spark/hard/anaconda3/bin/python3")
# contrib <- tf$contrib

tf$reset_default_graph()
# Import MNIST data using keras
mnist <- dataset_mnist()
TrainX <- mnist$train$x / 255.
TrainY <- mnist$train$y
TestX <- mnist$test$x / 255.
TestY <- mnist$test$y

# 28X28 image to 1X784 vector
TrainX <- matrix(TrainX, nrow = dim(TrainX)[1])
TestX <- matrix(TestX, nrow = dim(TestX)[1])

sample_ind_trx <- sample(1:nrow(TrainX), size = 5000)
sample_ind_tex <- sample(1:nrow(TestX), size = 200)

trX <- TrainX[sample_ind_trx,]
trY <- TrainY[sample_ind_trx]
teX <- TestX[sample_ind_tex,]
teY <- TestY[sample_ind_tex]

# Training and Test Graph Input
Xtr <- tf$placeholder(tf$float32, shape=shape(NULL, num_features))
Xte <- tf$placeholder(tf$float32, shape=shape(num_features))

# Nearest Neighbor calculation using L1 Distance
# Calculate L1 Distance
distance <- tf$reduce_sum(tf$abs(tf$subtract(Xtr, Xte)), reduction_indices = 1L)
# Prediction: Get min distance index (Nearest neighbor)
pred = tf$argmin(distance, 0L)

accuracy <- 0.

# Initialize the variables (i.e. assign their default value)
init <- tf$global_variables_initializer()

# Start training
with(tf$Session() %as% sess, {
  # Run the initializer
  sess$run(init)
  
  # loop over test data
  for (i in 1:nrow(teX)){
    # Get nearest neighbor
    nn_index <- sess$run(pred, feed_dict = dict(Xtr = trX, Xte = teX[i,]))
    # Get nearest neighbor class label and compare it to its true label
    print(paste("Test ", i, " Prediction: ", trY[nn_index], " True Class: ", teY[i]))
    # Calculate accuracy
    if (trY[nn_index] == teY[i]){
      accuracy <- accuracy + (1. / nrow(teX))
    }
  }
  print("Done!")
  print(paste("Accuracy: ", accuracy))
})





