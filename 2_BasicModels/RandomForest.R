# classify MNIST dataset using random forest with tensorflow
# reference : https://github.com/aymericdamien/TensorFlow-Examples/blob/master/examples/2_BasicModels/random_forest.py
library(magrittr)
library(tensorflow)
library(keras)
use_python("/home/spark/hard/anaconda3/bin/python3")
contrib <- tf$contrib
tensor_forest <- contrib$tensor_forest$python$tensor_forest

# Import MNIST data using keras
mnist <- dataset_mnist()
TrainX <- mnist$train$x / 255.
TrainY <- mnist$train$y
TestX <- mnist$test$x / 255.
TestY <- mnist$test$y

# 28X28 image to 1X784 vector
TrainX <- matrix(TrainX, nrow = dim(TrainX)[1])
TestX <- matrix(TestX, nrow = dim(TestX)[1])

# Parameters
num_steps = 500L # Total steps to train
batch_size = 1024L # The number of samples per batch
num_classes = 10L # The 10 digits
num_features = 784L # Each image is 28X28 pixels
num_trees = 50L
max_nodes = 1000L

# Input and Target data
X <- tf$placeholder(tf$float32, shape=shape(NULL, num_features))
# For random forest, labels must be integers (the class id)
Y <- tf$placeholder(tf$int32, shape=shape(NULL))

# Random Forest Parameters
hparams <- tensor_forest$ForestHParams(
  num_classes=num_classes,
  num_features=num_features,
  num_trees=num_trees,
  max_nodes=max_nodes
)$fill()

# Build the Random Forest
forest_graph <- tensor_forest$RandomForestGraphs(hparams)
# Get training graph and loss
train_op <- forest_graph$training_graph(X, Y)
loss_op <- forest_graph$training_loss(X, Y)

# Measure the accuracy
infer_op <- forest_graph$inference_graph(X)
correct_prediction <- tf$equal(tf$argmax(infer_op, 1L), tf$cast(Y, tf$int64))
accuracy_op <- correct_prediction %>% 
  tf$cast(., tf$float32) %>% 
  tf$reduce_mean()

# Initialize the variables (i.e. assign their default value)
init_vars <- tf$global_variables_initializer()

# Start Tensorflow session
sess <- tf$Session()

# Run the initializer
sess$run(init_vars)

# Training
for (i in 1:num_steps){
  # Prepare Data
  # Get the next_batch of MNIST data
  batch_idx <- sample(1:nrow(TrainX), size = batch_size)
  batch_x <- TrainX[batch_idx, ]
  batch_y <- TrainY[batch_idx]
  
  tmp <- sess$run(c(train_op, loss_op), feed_dict = dict(X = batch_x, Y = batch_y))
  l <- tmp[[2]]
  if (i %% 50 == 0 | i == 1){
    acc = sess$run(accuracy_op, feed_dict = dict(X = batch_x, Y = batch_y))
    print(paste("Step ", i, ", Loss:", l, ", Acc:", acc))
  }
}

# Test Model
print(paste("Test Accuracy: ", 
            round(sess$run(accuracy_op, feed_dict=dict(X = TestX, Y = TestY)), digits = 4)))
