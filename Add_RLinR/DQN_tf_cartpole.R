# Deep Q Network

library(magrittr)
library(tensorflow)
layers <- tf$layers
# use_python("/home/spark/hard/anaconda3/bin/python3")
# load reticulate package
library(reticulate)

# import gym module
gym <- import("gym")

# make environment : Breakout-v0
env <- gym$make("CartPole-v0")

# Hyperparameters
batch_size <- 50L # Batch size
gamma <- 0.95 # discounted factor
eps_start <- 1. # e-greedy initial value (p = 1)
eps_end <- 0.01 # e-greedy final value (p = 0.01)
eps_decay <- 5000 # decay steps
hidden_1 <- 100L # node of hidden layer 1
hidden_2 <- 100L # node of hidden layer 2
learning_rate <- 0.01 # learning rate
max_len <- 10000L # Replay memory size
steps_done <- 0L
frame <- 0L # Number of frames
num_episodes <- 200L
episode <- 0L
C_ <- 150 # Copy cycle
ep_rewards <- c()
max_q_values <- c()
mean_max_qvalues <- c()

# tensorflow graph
states_ <- tf$placeholder(dtype = tf$float32, shape = shape(NULL, env$observation_space$shape[[1]]))
actions_ <- tf$placeholder(dtype = tf$int32)
next_states_ <- tf$placeholder(dtype = tf$float32, shape = shape(NULL, env$observation_space$shape[[1]]))
rewards_ <- tf$placeholder(dtype = tf$float32)
dones_ <- tf$placeholder(dtype = tf$float32)

# Build model
Model <- function(states, scope, trainable = TRUE){
  with(tf$variable_scope(scope), {
    h_1 <- layers$dense(inputs = states, units = hidden_1, trainable = trainable,
                        activation = tf$nn$relu, name = "h_1", use_bias = FALSE)
    h_2 <- layers$dense(inputs = h_1, units = hidden_2, trainable = trainable, 
                        activation = tf$nn$relu, name = "h_2", use_bias = FALSE)
    out <- layers$dense(inputs = h_2, units = env$action_space$n, trainable = trainable,
                        name = "out", use_bias = FALSE) # Number of action is 2.
  })
  return(out)
}



# Define Main and Target network
MainNet <- Model(states = states_, scope = "main")
TargetNet <- Model(states = next_states_, scope = "target")

# Define training operation
train_op <- function(states, actions, next_states, rewards, dones, gamma, MainNet, TargetNet, batch_size){
  
  # discounted factor
  gamma_ <- tf$constant(gamma, dtype = tf$float32)
  
  # Q values with index by actions (using Main Network)
  indices_mat <- tf$constant(matrix(seq(from = 0L, to = as.integer(batch_size - 1L))), dtype = tf$int32)
  indices_mat <- tf$concat(c(indices_mat, actions), axis = 1L)
  state_action_values <- tf$gather_nd(MainNet, indices = indices_mat) %>% tf$reshape(shape = shape(-1L, 1L))
  
  # next max Q values (using Target Network)
  next_action_values <- tf$reduce_max(TargetNet, axis = 1L, keep_dims = T)
  
  # expected
  expected_state_action_values <- tf$multiply(gamma_, tf$multiply(next_action_values, 1 - dones_)) + rewards_
  
  # Define optimizer and Loss
  optimizer <- tf$train$AdamOptimizer(learning_rate = learning_rate)
  
  loss <- tf$reduce_sum(tf$square(expected_state_action_values - state_action_values))

  # Only update Main Network
  train <- optimizer$minimize(loss, 
                              var_list = tf$get_collection(tf$GraphKeys$TRAINABLE_VARIABLES, scope = "main"))
  return(c(train, loss))
}



train_and_loss <- train_op(states_, actions_, next_states_, rewards_, dones_, gamma, MainNet, TargetNet, batch_size)

# Copy to Main Network to Target Network
variables_init <- function(sess, main_scope, target_scope){
  main_ <- tf$get_collection(tf$GraphKeys$TRAINABLE_VARIABLES, scope = main_scope)
  target_ <- tf$get_collection(tf$GraphKeys$TRAINABLE_VARIABLES, scope = target_scope)
  for (i in 1:NROW(main_)){
    sess$run(target_[[i]]$assign(main_[[i]]))
  }
}


# Replay memory
states_list <- actions_list <- next_states_list <- rewards_list <- dones_list <- list()

# Configuration
config <- list()
config$gpu_options$allow_growth <- TRUE
session_conf <- do.call(tf$ConfigProto, config)

# session
sess <- tf$Session(config = session_conf)

# initializing all variables
init <- tf$global_variables_initializer()
sess$run(init)

# Copy main network to target network
variables_init(sess, "main", "target")

# DQN algorithms
for (i_ep in 1:num_episodes){
  ep_reward <- 0
  episode <- episode + 1L
  curr_frame <- env$reset()
  done <- FALSE
  while (done == FALSE){
    curr_state <- matrix(curr_frame, nrow = 1L)
    
    # Select Action by Epsilon Greedy
    sam_ <- runif(1)
    
    if (frame >= 500L){
      steps_done <- steps_done + 1
      eps_threshold <- eps_start + min(as.numeric(steps_done) / eps_decay, 1.) * (eps_end - eps_start)
      } else {
        eps_threshold <- eps_start
      }        
    if (sam_ > eps_threshold){
      state_action_value <- sess$run(MainNet, feed_dict = dict(states_ = curr_state))
      max_q_value <- max(state_action_value)
      max_q_values[i_ep] <- max_q_value
      action <- as.integer(state_action_value %>% which.max() - 1)
      } 
    else {
      action <- env$action_space$sample()
    }
    # env$render() ## display game
    after_step_infos <- env$step(action)
    next_frame <- after_step_infos[[1]]
    next_state <- matrix(next_frame, nrow = 1L)
    curr_frame <- next_frame
    reward <- after_step_infos[[2]]
    done <- after_step_infos[[3]]
    info <- after_step_infos[[4]]
    
    ep_reward <- ep_reward + reward
    
    if (done == TRUE){
      reward <- -50.
    }
    
    frame <- frame + 1L
    
    if (NROW(states_list) <= (max_len - 1)){
      states_list[[frame]] <- curr_state
      actions_list[[frame]] <- action
      next_states_list[[frame]] <- next_state
      rewards_list[[frame]] <- reward
      dones_list[[frame]] <- as.numeric(done)
    }
    
    if (NROW(states_list) > (max_len - 1)){
      states_list <- states_list[seq(to = NROW(states_list), length.out = (max_len - 1))]
      states_list[[NROW(states_list) + 1]] <- curr_state
      
      actions_list <- actions_list[seq(to = NROW(actions_list), length.out = (max_len - 1))]
      actions_list[[NROW(actions_list) + 1]] <- action
      
      next_states_list <- next_states_list[seq(to = NROW(next_states_list), length.out = (max_len - 1))]
      next_states_list[[NROW(next_states_list) + 1]] <- next_state
      
      rewards_list <- rewards_list[seq(to = NROW(rewards_list), length.out = (max_len - 1))]
      rewards_list[[NROW(rewards_list) + 1]] <- reward
      
      dones_list <- dones_list[seq(to = NROW(dones_list), length.out = (max_len - 1))]
      dones_list[[NROW(dones_list) + 1]] <- as.numeric(done)
      
    }
    
    if (frame >= 500L){
      batch_idx <- sample(1:NROW(states_list), size = batch_size)
      
      Batch <- 
        Map(function(x) Reduce(rbind, x), 
          list(states_list[batch_idx], 
               actions_list[batch_idx],
               next_states_list[batch_idx],
               rewards_list[batch_idx], 
               dones_list[batch_idx]))
      
      feed_dict <- 
        dict(
          states_ = Batch[[1]],
          actions_ = Batch[[2]],
          next_states_ = Batch[[3]],
          rewards_ = Batch[[4]],
          dones_ = Batch[[5]]
        )
      
      ttmp <- sess$run(train_and_loss, feed_dict = feed_dict)
      
      if (frame %% C_ == 0){
        # sess$run(ass_ops)
        variables_init(sess, "main", "target")
        print("Target Network Update !!")
        gc()
      }
    }
  }
  
  ep_rewards[i_ep] <- ep_reward
  mean_max_qvalues[i_ep] <- mean(max_q_values, na.rm = T)
  if (episode >= 10){
    print(paste0("Episode ", episode, 
                 ", Frame : ", frame, 
                 ", Max Q Value : ", if(!(is.null(mean_max_qvalues[i_ep]))) round(mean_max_qvalues[i_ep], digits = 5L),
                 ", E-Greedy : ", round(eps_threshold, digits = 5L), 
                 ", Scores : ", ep_reward))
  }
  
  if (i_ep >= 100){
    if (mean(ep_rewards[seq(to = i_ep, length.out = 100)]) > 190){
      break
    }
  }
}


par(mfrow = c(2,1))
plot(ep_rewards, type = "l", main = "Scores each episode in CartPole-v0", col = 1, 
     ylab = "Score")
plot(mean_max_qvalues, type = "l", main = "Mean Max Q value each episode in CartPole-v0", col = 2,
     ylab = "Max Q value")


